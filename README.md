# alpine-emby

# [alpine-x64-emby](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-emby/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinex64build/alpine-x64-emby.svg)](https://microbadger.com/images/forumi0721alpinex64build/alpine-x64-emby "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinex64build/alpine-x64-emby.svg)](https://microbadger.com/images/forumi0721alpinex64build/alpine-x64-emby "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64build/alpine-x64-emby.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-emby/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64build/alpine-x64-emby.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-emby/)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64
* Appplication : [Emby](https://emby.media/)
    - Emby is designed to help you manage your personal media library, such as home videos and photos.
* Base Image
    - [forumi0721/alpine-x64-base](https://hub.docker.com/r/forumi0721/alpine-x64-base/)



----------------------------------------
#### Run

* x64
```sh
docker run -d \
		   -p 8096:8096/tcp \
		   -p 8920:8920/udp \
		   -v /comf.d:/conf.d \
		   -v /data:/data \
		   forumi0721alpinex64build/alpine-x64-emby:latest
```



----------------------------------------
#### Usage

* URL : [http://localhost:8096/](http://localhost:8096/)



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| --net=host         | for Broadcast                                    |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 8096/tcp           | http port                                        |
| 8920/tcp           | https port                                       |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| /conf.d            | Config data                                      |
| /data              | Media data                                       |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |



----------------------------------------
* [forumi0721alpinex64build/alpine-x64-emby](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-emby/)

